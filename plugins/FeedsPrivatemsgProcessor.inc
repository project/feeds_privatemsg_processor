<?php

/**
 * @file
 * FeedsPrivatemsgProcessor class.
 */

/**
 * Creates private messages from feeds items.
 *
 * Updates and deletions of feeds items are not supported.
 */
class FeedsPrivatemsgProcessor extends FeedsProcessor {

  /**
   * Define the entity type.
   */
  public function entityType() {
    return 'privatemsg_message';
  }

  /**
   * Overrides parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label'] = t('Private message');
    $info['label plural'] = t('Private messages');
    return $info;
  }

  /**
   * Updates are not supported.
   */
  protected function entityLoad(FeedsSource $source, $mid) {
    return NULL;
  }

  /**
   * Validate a private message.
   */
  protected function entityValidate($entity) {
    parent::entityValidate($entity);

    if (!isset($entity->author)) {
      throw new FeedsValidationException(t('The author is not defined.'));
    }

    if (isset($entity->parent_mid)) {
      // Reset the entity cache.
      // @see https://www.drupal.org/node/2292111
      entity_get_controller('privatemsg_message')->resetCache(array($entity->parent_mid));

      $parent_message = privatemsg_message_load($entity->parent_mid, $entity->author);
      if (empty($parent_message)) {
        throw new FeedsValidationException(t('The parent private message MID is invalid.'));
      }
      if (isset($entity->thread_id) && $entity->thread_id != $parent_message->thread_id) {
        throw new FeedsValidationException(t('The thread private message MID is invalid.'));
      }
      $entity->thread_id = $parent_message->thread_id;
    }
    elseif (isset($entity->thread_id)) {
      // Reset the entity cache.
      entity_get_controller('privatemsg_message')->resetCache(array($entity->thread_id));

      $thread_message = privatemsg_message_load($entity->thread_id, $entity->author);
      if (empty($thread_message)) {
        throw new FeedsValidationException(t('The thread private message MID is invalid.'));
      }
    }
    // Verify whether inserting a new thread is enabled.
    elseif (!$this->config['insert_threads']) {
      throw new FeedsValidationException(t('A thread or parent private message MID is required.'));
    }
    // Required for a new thread.
    else {
      if (empty($entity->recipients)) {
        throw new FeedsValidationException(t('The recipients are invalid.'));
      }
      if (empty($entity->message_subject)) {
        throw new FeedsValidationException(t('The message subject is empty.'));
      }
    }

    if (empty($entity->message_body)) {
      throw new FeedsValidationException(t('The message body is empty.'));
    }

  }

  /**
   * Overrides parent::entitySaveAccess().
   *
   * @see entitySave()
   * @see privatemsg_reply()
   * @see _privatemsg_validate_message()
   */
  protected function entitySaveAccess($entity) {
  }

  /**
   * Save the private message.
   */
  protected function entitySave($entity) {
    $options = array(
      'author' => $entity->author,
    );

    if (isset($entity->timestamp)) {
      $options['timestamp'] = $entity->timestamp;
    }

    // @see hook_feeds_processor_targets()
    // @see hook_feeds_presave()
    if (isset($entity->format)) {
      $options['format'] = $entity->format;
    }

    // @see feeds_privatemsg_processor_privatemsg_message_insert()
    // @see feeds_privatemsg_processor_privatemsg_message_flush()
    if (isset($entity->feeds_item)) {
      $options['feeds_item'] = $entity->feeds_item;
    }

    // Reply.
    if (isset($entity->thread_id)) {
      // Reset the entity cache.
      // @see https://www.drupal.org/node/2292111
      entity_get_controller('privatemsg_message')->resetCache(array($entity->thread_id));

      $validated = privatemsg_reply($entity->thread_id, $entity->message_body, $options);
    }
    // New thread.
    else {
      $validated = privatemsg_new_thread($entity->recipients, $entity->message_subject, $entity->message_body, $options);
    }

    if (!$validated['success']) {
      if (isset($validated['messages']['error'])) {
        $error_message = implode("\n", $validated['messages']['error']);
      }
      else {
        $error_message = t('There was an error while validating the private message.');
      }

      throw new FeedsValidationException($error_message);
    }
    elseif (isset($validated['message']) && $validated['message'] === FALSE) {
      $error_message = t('There was an error while importing the private message.');

      throw new FeedsValidationException($error_message);
    }
  }

  /**
   * Overrides parent::createLogEntry().
   */
  protected function createLogEntry(Exception $e, $entity, $item) {
    // Validation failed hook.
    module_invoke_all('feeds_privatemsg_processor_validation_failed', $entity, $item, $e->getMessage());

    return parent::createLogEntry($e, $entity, $item);
  }

  /**
   * Overrides parent::existingEntityId().
   */
  protected function existingEntityId(FeedsSource $source, FeedsParserResult $result) {
    // Before searching for an existing ID, ensure at least one of the unique
    // targets has a value.
    $values = $this->uniqueTargets($source, $result);
    $values = array_filter($values);

    if (empty($values)) {
      return 0;
    }

    return parent::existingEntityId($source, $result);
  }

  /**
   * Deletions are not supported.
   */
  protected function entityDeleteMultiple($mids) {
  }

  /**
   * Not supported.
   */
  protected function initEntitiesToBeRemoved(FeedsSource $source, FeedsState $state) {
    $state->removeList = array();
  }

  /**
   * Not supported.
   */
  protected function clean(FeedsState $state) {
  }

  /**
   * Not supported.
   */
  public function clear(FeedsSource $source) {
    drupal_set_message(t('This processor does not support deletions.'));
  }

  /**
   * Overrides parent::configDefaults().
   */
  public function configDefaults() {
    $defaults = parent::configDefaults();

    $defaults['insert_threads'] = FALSE;

    return $defaults;
  }

  /**
   * Overrides parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);

    unset($form['update_existing']);
    unset($form['skip_hash_check']);
    unset($form['input_format']);
    unset($form['update_non_existent']);

    $form['insert_threads'] = array(
      '#type' => 'checkbox',
      '#title' => t('Insert new threads'),
      '#description' => t('Whether a new thread is created in case no reference to a thread is found.'),
      '#default_value' => $this->config['insert_threads'],
    );

    return $form;
  }

  /**
   * Overrides parent::setTargetElement().
   */
  public function setTargetElement(FeedsSource $source, $target_message, $target_element, $value) {
    switch ($target_element) {
      case 'author_uid':
        if ($account = user_load($value)) {
          $target_message->author = $account;
        }
        break;

      case 'author_name':
        if ($account = user_load_by_name($value)) {
          $target_message->author = $account;
        }
        break;

      case 'author_mail':
        if ($account = user_load_by_mail($value)) {
          $target_message->author = $account;
        }
        break;

      case 'parent_guid':
        if ($mid = $this->findEntityIdByGuid($value)) {
          $target_message->parent_mid = $mid;
        }
        break;

      case 'recipients_by_uid':
        // @see FeedsUserProcessor::setTargetElement()
        // @see feeds_privatemsg_processor_tests_feeds_after_parse()
        $value = (array) $value;
        if ($recipients = $this->loadRecipients($value)) {
          $target_message->recipients = $recipients;
        }
        break;

      case 'recipients_by_name':
        $value = (array) $value;
        if ($recipients = $this->loadRecipients($value, 'name')) {
          $target_message->recipients = $recipients;
        }
        break;

      case 'recipients_by_mail':
        $value = (array) $value;
        if ($recipients = $this->loadRecipients($value, 'mail')) {
          $target_message->recipients = $recipients;
        }
        break;

      case 'timestamp':
        $target_message->timestamp = feeds_to_unixtime($value, REQUEST_TIME);
        break;

      default:
        parent::setTargetElement($source, $target_message, $target_element, $value);
        break;
    }
  }

  /**
   * Find an entity ID by GUID.
   */
  protected function findEntityIdByGuid($guid) {
    if (empty($guid)) {
      return FALSE;
    }

    return db_select('feeds_item')
      ->fields('feeds_item', array('entity_id'))
      ->condition('entity_type', $this->entityType())
      ->condition('guid', $guid)
      ->range(0, 1)
      ->execute()
      ->fetchField();
  }

  /**
   * Load a list of recipients.
   *
   * @param array $values
   *   The array representing the recipients.
   * @param string $field
   *   (optional) The table field name used to find the recipients. Defaults to
   *   'uid'.
   *
   * @return array
   *   Returns an array containing the successfully loaded recipients.
   */
  protected function loadRecipients($values, $field = 'uid') {
    if (empty($values)) {
      return array();
    }
    if (empty($field)) {
      return array();
    }

    $uids = array();

    // Load by UID.
    if ($field == 'uid') {
      foreach ($values as $value) {
        $uid = intval($value);
        if ($uid > 0) {
          $uids[$uid] = $uid;
        }
      }
      $uids = array_keys($uids);
    }
    // Load by field.
    else {
      $uids = db_select('users')
        ->fields('users', array('uid'))
        ->condition($field, $values, 'IN')
        ->condition('uid', 0, '>')
        ->execute()
        ->fetchCol();
    }

    if (empty($uids)) {
      return array();
    }

    return user_load_multiple($uids);
  }

  /**
   * Overrides parent::getMappingTargets().
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();

    $targets['author_uid'] = array(
      'name' => t('Author UID'),
      'description' => t('The identifier of the author.'),
      'real_target' => 'author',
    );

    $targets['author_name'] = array(
      'name' => t('Author Name'),
      'description' => t('The name of the author.'),
      'real_target' => 'author',
    );

    $targets['author_mail'] = array(
      'name' => t('Author e-mail'),
      'description' => t('The e-mail address of the author.'),
      'real_target' => 'author',
    );

    $targets['thread_id'] = array(
      'name' => t('Thread private message MID'),
      'description' => t('The identifier of the thread private message.'),
    );

    $targets['parent_mid'] = array(
      'name' => t('Parent private message MID'),
      'description' => t('The identifier of the parent private message.'),
    );

    $targets['parent_guid'] = array(
      'name' => t('Parent private message GUID'),
      'description' => t('The GUID identifier of the parent private message.'),
      'real_target' => 'parent_mid',
    );

    $targets['recipients_by_uid'] = array(
      'name' => t('Recipients by UID'),
      'description' => t('The recipients as a list of account UID.'),
      'real_target' => 'recipients',
    );

    $targets['recipients_by_name'] = array(
      'name' => t('Recipients by name'),
      'description' => t('The recipients as a list of account names.'),
      'real_target' => 'recipients',
    );

    $targets['recipients_by_mail'] = array(
      'name' => t('Recipients by mail'),
      'description' => t('The recipients as a list of account mails.'),
      'real_target' => 'recipients',
    );

    $targets['timestamp'] = array(
      'name' => t('Sent date'),
      'description' => t('The sent date of the private message.'),
    );

    $targets['message_subject'] = array(
      'name' => t('Message subject'),
      'description' => t('The subject of the private message.'),
    );

    $targets['message_body'] = array(
      'name' => t('Message body'),
      'description' => t('The body of the private message.'),
    );

    $this->getHookTargets($targets);

    return $targets;
  }

}
