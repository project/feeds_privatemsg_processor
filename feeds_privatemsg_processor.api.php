<?php

/**
 * @file
 * Hooks provided by the feeds_privatemsg_processor module.
 */

/**
 * Respond to FeedsPrivatemsgProcessor validation errors.
 *
 * @param object $entity
 *   The feeds entity which failed validation.
 * @param array $item
 *   The current FeedsParserResult item.
 * @param string $error_message
 *   The error message.
 *
 * @see FeedsPrivatemsgProcessor::createLogEntry()
 */
function hook_feeds_privatemsg_processor_validation_failed($entity, $item, $error_message) {
}
